'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _XorCracker = require('./XorCracker.js');

var _XorCracker2 = _interopRequireDefault(_XorCracker);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; } // import and init XorCracker function

// import read/write file

// get current working directory


function XorCrackerService() {}

/**
 * Promise all for the read file
 */
function promiseAllP(items, block) {
    var promises = [];
    items.forEach(function (item, index) {
        promises.push(function (item, i) {
            return new Promise(function (resolve, reject) {
                return block.apply(this, [item, index, resolve, reject]);
            });
        }(item, index));
    });
    return Promise.all(promises);
}

/**
 * Read all files from a directory
 */
function readFiles(dirname, encoding) {
    return new Promise(function (resolve, reject) {
        _fs2.default.readdir(dirname, function (err, filenames) {
            if (err) return reject(err);
            promiseAllP(filenames, function (filename, index, resolve, reject) {
                // Load the file in binary
                _fs2.default.readFile(_path2.default.resolve(dirname, filename), encoding, function (err, content) {
                    return resolve({ filename: filename, contents: content });
                });
            }).then(function (results) {
                return resolve(results);
            }).catch(function (error) {
                return reject(error);
            });
        });
    });
}

/**
 * Detect the most recurring key from frequency analysis
 */
function countDuplicates(array) {
    "use strict";

    var result = {};
    // Check if input is array
    if (array instanceof Array) {
        array.forEach(function (v, i) {
            // Initial object property creation
            if (!result[v]) {
                // Create an array for that property
                result[v] = 1;
                // Same occurrences found
            } else {
                // Fill the array
                result[v]++;
            }
        });
    }
    return result;
}

/**
 * Matches words in text with dictionary and outputs %
 * @param {*} text
 */
function dictionaryChecker(textByLine, dicoByLine) {
    var matched = 0;
    var total = 0;
    for (var i = 0, j = textByLine.length; i < j; i++) {
        var cur = textByLine[i];
        total++;
        if (dicoByLine.includes(cur)) {
            matched++;
        }
    }
    return matched / total * 100;
}

/**
 * Formats the dictionary into an array of words 
 *
 * @param {*} dictionary
 * @returns
 */
function formatDictionary(dictionary) {
    // Remove potential \r from windows line break
    dictionary = dictionary.replace(/\r/g, '');
    // Remove line break
    return dictionary.split("\n");
}

/**
 * Formats the text into an array of words
 *
 * @param {*} text
 */
function formatText(text) {
    // Remove all punctuation
    var textByLine = text.replace(/[\.,-\/#!$%\^&\*;:{}=\-_`~()@\+\?><\[\]\+]/g, '');
    // Match all the possible words into an array (with accents)
    textByLine = textByLine.match(/[A-Za-z0-9çáêèéíóúùôîï'\-]+/gi);
    // Go through all the words to check if there is an apostrophe and remove what precedes
    textByLine.forEach(function (word, index) {
        if (word.indexOf('qu\'') === 1) {
            textByLine[index] = word.substr(3);
        } else if (word.indexOf('\'') === 1) {
            textByLine[index] = word.substr(2);
        }
    });
    // Return the formatted array of words
    return textByLine;
}

/**
 * Main funtion
 */
XorCrackerService.prototype.decipher = function () {
    var tryChars = 32;
    var myFiles = [];
    var myDictionaries = [];
    var keys = [];
    // Set the key length to 6 chars as mentioned in the hacker note
    _XorCracker2.default.setKeyLength(6);
    // Load the dictionaries from the "dictionary" folder
    readFiles('dictionaries/', 'utf8').then(function (files) {
        // Loop through the file to do a frequency analysis and guess the key
        files.forEach(function (item, index) {
            // Push the file into an array to loop through it again later
            myDictionaries.push({ filename: item.filename, contents: formatDictionary(item.contents) });
        });
        // Read the encrypted files
        return readFiles('encrypted', 'binary');
    }).then(function (files) {
        // Loop through the file to do a frequency analysis and guess the key
        files.forEach(function (file, index) {
            // Push the file into an array to loop through it again later
            myFiles.push(file);
            // Set the file for the cracker
            _XorCracker2.default.setFile(file.contents);
            // Store the key in an array
            keys.push(_XorCracker2.default.guessKeys(_XorCracker2.default.file, tryChars));
        });
    }).then(function () {
        // Flatten the key array
        var merged = [].concat.apply([], keys);
        // Count duplicated keys
        var duplicates = countDuplicates(merged);
        // Loop again through the files and decrypt all of them using the probable key
        var fileKeyPercent = {};
        // Decrypt with each key and check dictionary for any match
        Object.keys(duplicates).forEach(function (key) {
            myFiles.forEach(function (file) {
                // Set the file for the xor cracker
                _XorCracker2.default.setFile(file.contents);
                // Decrypt the file
                var decryptedFile = _XorCracker2.default.decryptFile(_XorCracker2.default.file, key);
                var wordArrayFromDecrypt = formatText(decryptedFile);
                // Loop through the dictionaries
                myDictionaries.forEach(function (dictionary) {
                    // Check with dictionary (returns matched and total)
                    var decryptChecker = dictionaryChecker(wordArrayFromDecrypt, dictionary.contents);
                    console.log(file.filename, key, dictionary.filename, decryptChecker);
                    // insert data into var to be compared later (set first if empty)
                    if (typeof fileKeyPercent[file.filename] === 'undefined') {
                        fileKeyPercent[file.filename] = _defineProperty({}, key, { dictionary: dictionary.filename, percentage: decryptChecker, decrypted: decryptedFile });
                    } else if (typeof fileKeyPercent[file.filename][key] === 'undefined' || fileKeyPercent[file.filename][key].percentage < decryptChecker) {
                        fileKeyPercent[file.filename][key] = { dictionary: dictionary.filename, percentage: decryptChecker, decrypted: decryptedFile };
                    }
                });
            });
        });
        var log = '';
        // Loop through the files data corresponding to the correct dictionary, percentage, key used and decrypted text
        Object.keys(fileKeyPercent).forEach(function (fileName) {
            // Attribute value to variable for easier use
            var fileDataPercent = fileKeyPercent[fileName];
            // Only keep the key of the higher percentage of match with a key
            var keyUsed = Object.keys(fileDataPercent).reduce(function (a, b) {
                return fileDataPercent[a].percentage > fileDataPercent[b].percentage ? a : b;
            });
            // Get the file data from the higher percentage of match
            var higherMatch = fileDataPercent[keyUsed];
            // Output the decrypted file in a separate file
            _fs2.default.writeFile("./decrypted/" + fileName, higherMatch.decrypted, function (err) {
                if (err) {
                    // Error logging if file writing doesn't work
                    return console.log(err);
                }
                // Logging success
                console.log("The file", fileName, "was saved!");
            });
            log += fileName + ':\r\n\tKey used: ' + keyUsed + '\r\n\tDictionary used: ' + higherMatch.dictionary + '\r\n\tPercentage of match: ' + higherMatch.percentage + ':\r\n\r\n';
        });
        // Ouput the key and full log in the file
        _fs2.default.writeFile("./decrypted/log.txt", log, function (err) {
            if (err) {
                // Error logging if file writing doesn't work
                return console.log(err);
            }
            // Logging success
            console.log("The file log.txt was saved!");
        });
    })
    // Error logging
    .catch(function (error) {
        console.log(error);
    });
};

exports.default = new XorCrackerService();