"use strict";

var _XorCrackerService = require("./XorCrackerService.js");

var _XorCrackerService2 = _interopRequireDefault(_XorCrackerService);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Runs the  code when executing the file with npm start
_XorCrackerService2.default.decipher(); // import and init XorCracker function