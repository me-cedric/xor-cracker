"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});
function XorCracker() {
	this.maxKeyLength = 65;
	this.file;
	this.knownKeyLength;
}

/**
 * Set the file
 */
XorCracker.prototype.setFile = function (file) {
	this.file = file;
};

/**
 * Set the key length
 */
XorCracker.prototype.setKeyLength = function (keyLength) {
	this.knownKeyLength = keyLength;
};

/**
 * Frequency analysis - count char offset
 */
XorCracker.prototype.charsCountAtOffset = function (text, keyLength, offset) {
	var charsCount = new Array();
	for (var pos = offset; pos < text.length; pos += keyLength) {
		var c = text.substr(pos, 1);
		if (c in charsCount) {
			charsCount[c] += 1;
		} else {
			charsCount[c] = 1;
		}
	}
	return charsCount;
};

/**
 * Decrypt the file using a key
 */
XorCracker.prototype.decryptFile = function (text, key) {
	var raw = this.xorEncode(text, key);
	return raw;
};

/**
 * Guess the probable key for an encrypted text using frequency analysis
 */
XorCracker.prototype.guessKeys = function (text, mostChar) {
	var keyLength = this.knownKeyLength;
	var keyPossibleBytes = new Array();
	for (var i = 0; i < keyLength; i++) {
		keyPossibleBytes.push(new Array());
	}
	for (var offset = 0; offset < keyLength; offset++) {
		var charsCount = this.charsCountAtOffset(text, keyLength, offset);
		var maxCount = Math.max.apply(Math, Object.keys(charsCount).map(function (n) {
			return charsCount[n];
		}));
		for (var c in charsCount) {
			if (charsCount[c] >= maxCount) {
				keyPossibleBytes[offset].push(String.fromCharCode(c.charCodeAt(0) ^ mostChar));
			}
		}
	}
	return this.allKeys(keyPossibleBytes);
};

/**
 * Decode all the keys from byte to byte frequency analysis
 */
XorCracker.prototype.allKeys = function (keyPossibleBytes, keyPart, offset) {
	keyPart = typeof keyPart !== "undefined" ? keyPart : "";
	offset = typeof offset !== "undefined" ? offset : 0;
	var keys = new Array();
	if (offset >= keyPossibleBytes.length) {
		return [keyPart];
	}
	for (var c in keyPossibleBytes[offset]) {
		keys = keys.concat(this.allKeys(keyPossibleBytes, keyPart + keyPossibleBytes[offset][c], offset + 1));
	}
	return keys;
};

/**
 * XOR application function for xor decryption and encryption using a key
 */
XorCracker.prototype.xorEncode = function (text, key) {
	var keyLength = key.length;
	var output = "";
	for (var i = 0; i < text.length; i++) {
		output += String.fromCharCode(text[i].charCodeAt() ^ key[i % keyLength].charCodeAt());
	}
	return output;
};

exports.default = new XorCracker();