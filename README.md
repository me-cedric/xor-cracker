# xor cracker

Run `npm install` to install the required module for the project to work.
The project is written in ES6 and translated for `node` through `babel` in the `dist` directory.

## How to run
`npm start`

## Input
Put the files in the root of the encrypted folder.

## Output
The `decrypted` directory contains the output for each file and a `log.txt` detailing the results.