ad
ante
apud
circum
inter
per
post
pro
sine
diu
denique
etiam
magis
modo
multum
mox
nimis
nonne
num
primum
qua
quo
satis
simul
tam
tantum
tot
tunc
ubi
unde
nec
neque
at
autem
dum
igitur
ne
postquam
quoniam
sicut
ut
vel
velut
vero
Alius
alter
idem
ipse
quid
quis
amicitia
amicus
amor
arbor
ars
audacia
aurum
auxilium
avaritia
beneficium
benevolentia
caput
carmen
casus
cena
certamen
circus
clamor
cliens
comes
comitium
convivium
copia
cupiditas
cura
currus
cursus
desiderium
dextera
diligentia
discipulus
divitiae
dolor
dominus
donum
eques
exercitus
fabula
factum
fas
nefas
fatum
flamma
fortuna
fuga
furor
gaudium
genus
gratia
gravitas
habitus
honor
hospes
humanitas
imperium
impetus
industria
inimicus
injuria
inopia
invidia
ira
jus
justitia
labor
lacrima
laetititia
libertus
litus
lucus
ludus
lumen
lux
magister
majores
manus
mens
metus
mons
mores
mors
mos
munus
natio
natura
negotium
nemus
nobilitas
numerus
oculus
odium
officium
onus
opera
ops
orbis
ordo
os
otium
parens
patronus
paupertas
pectus
pecunia
periculum
plebs
poeta
potestas
praemium
princeps
provincia
prudentia
pudor
quies
Quirites
regio
ripa
saevitia
salus
sanguis
sapientia
scelus
sermo
sol
solitudo
sollicitudo
somnus
spatium
spectaculum
spes
studium
superbia
telum
theatrum
triumphus
turba
umbra
usus
utilitas
vates
verbum
vestis
vicinus
vinum
vis
voluptas
vox
vulgus
vulnus
vultus
aequalis
alienus
altus
audax
avarus
avidus
barbarus
beatus
benignus
brevis
carus
celer
certus
ceteri
crudelis
facilis
falsus
fessus
fidelis
gratus
gravis
honestus
humanus
impar
impiger
improbus
imus
ingens
iratus
jucundus
laetus
lentus
levis
liber
longus
medius
mille
miser
nobilis
noster
novem
novus
octo
otiosus
par
patricius
pauper
perfidus
piger
plenus
plerique
primus
probus
propinquus
propior
proximus
quatuor
quinque
saevus
secundus
securus
septem
sex
solus
summus
superbus
superus
suus
tertius
totus
tres
tristis
turpis
tutus
unus
validus
vastus
velox
vester
vetus
absum
accipio
adsum
amitto
aspicio
audeo
cado
cano
careo
caveo
claudo
cognosco
cogo
conficio
contemno
cupio
curro
debeo
decerno
delecto
desidero
desum
diligo
disco
doceo
doleo
duco
egeo
emo
eripio
excipio
existimo
faveo
fero
fingo
fio
fleo
frango
gaudeo
impero
incipio
invideo
jaceo
judico
jungo
juvo
laboro
licet
ludo
maneo
moneo
narro
nego
nescio
nolo
nosco
occido
occupo
odi
oportet
opto
ostendo
parco
pareo
paro
pelo
perdo
permitto
pervenio
placeo
praebeo
praesto
prohibeo
prosum
quaero
quiesco
recipio
reddo
redeo
refero
relinquo
respondeo
rumpo
scio
scribo
sentio
soleo
specto
spero
statuo
sumo
taceo
tango
tego
terreo
timeo
tollo
traho
trado
valeo
veho
verto
veto
adversus
causa
contra
gratia
ob
obviam
propter
sub
super
adeo
certe
ceterum
diligenter
equidem
frustra
immo
inde
interea
libenter
paene
potius
procul
profecto
quidem
retro
rursus
sane
scilicet
statim
tandem
undique
usque
utinam
utruman
vehementer
vix
numquam
ergo
quedmadmodum
quomodo
Aliquis
nemo
nihil
nullus
uterque
quidam
quisquam
quisque
actio
admiratio
adventus
aedilis
ambitio
ambitus
anima
arbitrium
argumentum
artifex
artificium
auctor
auctoritas
auris
boni
brevitas
censor
clementia
colloqium
color
concilium
concordia
consuetudo
contio
copia
crimen
culpa
cultus
cupido
decus
dignitas
disciplina
discordia
discrimen
eloquentia
epistula
error
exemplum
exercitatio
exordium
exsilium
facinus
facultas
flagitium
foedus
forma
fraus
ingenium
initium
insidiae
judex
judicium
justitia
lex
liber
libertas
libido
licentia
limen
limes
lingua
littera
magistratus
mediocritas
memoria
minae
modus
molestia
multitudo
narratio
opinio
oratio
orator
partes
peroratio
persona
pietas
poena
praesidium
praetor
pretium
principium
prudentia
pulchritudo
quaestio
quaestor
ratio
recitatio
reus
sententia
sermo
signum
simulacrum
species
stirps
studium
temperentia
testis
tribunus
venia
veritas
veteres
virtus
vis
voluntas
acer
acerbus
acutus
aequus
amplus
antiquus
aptus
asper
blandus
calidus
candidus
celeber
civilis
conscius
copiosus
creber
disertus
dubius
eruditus
humilis
ignarus
immanis
ingeniosus
insignis
integer
lenis
liberalis
maestus
mollis
memor
mirus
necessarius
nescius
notus
peritus
placidus
popularis
potens
prudens
publicus
rectus
rudis
sapiens
(dis)similis
sollers
sollicitus
suavis
supplex
tacitus
tener
tenuis
uber
urbanus
utilis
varius
accedo
addo
adhibeo
admoneo
agnosco
aggredior
agito
animadverto
aperio
arbitror
augeo
castigo
censeo
clamo
compono
confero
confirmo
confiteor
conor
conspicio
consuesco
contendo
corrumpo
decet
decipio
defendo
desino
disputo
dissero
dubito
exerceo
exspecto
fallo
flecto
fluo
fundo
hortor
ignoro
incedo
incendo
ingredior
intellego
interogo
invenio
irascor
juro
lenio
libet
loquor
memoro
miror
miseret
moveo
nascor
necesseest
neglego
nitor
noceo
nuntio
observo
offendo
orior
orno
paenitet
patior
perspicio
pertineo
peto
pingo
placo
posco
praecipio
praetereo
precor
probo
proficiscor
propono
pudet
queror
rego
reprehendo
sequor
consequor
servio
solvo
studeo
suadeo
persuadeo
suscipio
tribuo
tueor
urgeo
utor
vereor
versor
vindico
vito
vitupero
volvo
ad
ante
apud
circum
inter
per
post
pro
sine
diu
denique
etiam
magis
modo
multum
mox
-ne
nimis
nonne
num
primum
qua
quo
satis
simul
tam
tantum
tot
tunc
ubi
unde
nec/neque
at
autem
dum
igitur
ne
postquam
quoniam
sicut
ut
vel
velut
vero
Alius
alter
idem
ipse
quid?
quis?
amicitia
amicus
amor
arbor
ars
audacia
aurum
auxilium
avaritia
beneficium
benevolentia
caput
carmen
casus
cena
certamen
circus
clamor
cliens
comes
comitium
convivium
copia
cupiditas
cura
currus
cursus
desiderium
dextera
diligentia
discipulus
divitiae
dolor
dominus
donum
eques
exercitus
fabula
factum
fas
nefas
fatum
flamma
fortuna
fuga
furor
gaudium
genus
gratia
gravitas
habitus
honor
hospes
humanitas
imperium
impetus
industria
inimicus
injuria
inopia
invidia
ira
jus
justitia
labor
lacrima
laetititia
libertus
litus
lucus
ludus
lumen
lux
magister
majores
manus
mens
metus
mons
mores
mors
mos
munus
natio
natura
negotium
nemus
nobilitas
numerus
oculus
odium
officium
onus
opera
ops
orbis
ordo
os
otium
parens
patronus
paupertas
pectus
pecunia
periculum
plebs
poeta
potestas
praemium
princeps
provincia
prudentia
pudor
quies
Quirites
regio
ripa
saevitia
salus
sanguis
sapientia
scelus
sermo
sol
solitudo
sollicitudo
somnus
spatium
spectaculum
spes
studium
superbia
telum
theatrum
triumphus
turba
umbra
usus
utilitas
vates
verbum
vestis
vicinus
vinum
vis
voluptas
vox
vulgus
vulnus
vultus
aequalis
alienus
altus
audax
avarus
avidus
barbarus
beatus
benignus
brevis
carus
celer
certus
ceteri
crudelis
facilis
falsus
fessus
fidelis
gratus
gravis
honestus
humanus
impar
impiger
improbus
imus
ingens
iratus
jucundus
laetus
lentus
levis
liber
longus
medius
mille
miser
nobilis
noster
novem
novus
octo
otiosus
par
patricius
pauper
perfidus
piger
plenus
plerique
primus
probus
propinquus
propior
proximus
quatuor
quinque
saevus
secundus
securus
septem
sex
solus
summus
superbus
superus
suus
tertius
totus
tres
tristis
turpis
tutus
unus
validus
vastus
velox
vester
vetus
absum
accipio
adsum
amitto
aspicio
audeo
cado
cano
careo
caveo
claudo
cognosco
cogo
conficio
contemno
cupio
curro
debeo
decerno
delecto
desidero
desum
diligo
disco
doceo
doleo
duco
egeo
emo
eripio
excipio
existimo
faveo
fero
fingo
fio
fleo
frango
gaudeo
impero
incipio
invideo
juvo
laboro
licet
ludo
maneo
moneo
narro
nego
nescio
nolo
nosco
occido
occupo
odi
oportet
opto
ostendo
parco
pareo
paro
pelo
perdo
permitto
pervenio
placeo
praebeo
praesto
prohibeo
prosum
quaero
quiesco
recipio
reddo
redeo
refero
relinquo
respondeo
rumpo
scio
scribo
sentio
soleo
specto
spero
statuo
sumo
taceo
tango
tego
terreo
timeo
tollo
traho
trado
valeo
veho
verto
veto
Proinde
concepta
rabie
saeviore
quam
desperatio
incendebat
et
fames
amplificatis
viribus
ardore
incohibili
in
excidium
urbium
matris
Seleuciae
efferebantur
quam
comes
tuebatur
Castricius
tresque
legiones
bellicis
sudoribus
induratae
Haec
ubi
latius
fama
vulgasset
missaeque
relationes
adsiduae
Gallum
Caesarem
permovissent
quoniam
magister
equitum
longius
ea
tempestate
distinebatur
iussus
comes
orientis
Nebridius
contractis
undique
militaribus
copiis
ad
eximendam
periculo
civitatem
amplam
et
oportunam
studio
properabat
ingenti
quo
cognito
abscessere
latrones
nulla
re
amplius
memorabili
gesta
dispersique
ut
solent
avia
montium
petiere
celsorum
Proinde
concepta
rabie
saeviore
quam
desperatio
incendebat
et
fames
amplificatis
viribus
ardore
incohibili
in
excidium
urbium
matris
Seleuciae
efferebantur
quam
comes
tuebatur
Castricius
tresque
legiones
bellicis
sudoribus
induratae
Haec
ubi
latius
fama
vulgasset
missaeque
relationes
adsiduae
Gallum
Caesarem
permovissent
quoniam
magister
equitum
longius
ea
tempestate
distinebatur
iussus
comes
orientis
Nebridius
contractis
undique
militaribus
copiis
ad
eximendam
periculo
civitatem
amplam
et
oportunam
studio
properabat
ingenti
quo
cognito
abscessere
latrones
nulla
re
amplius
memorabili
gesta
dispersique
ut
solent
avia
montium
petiere
celsorum
jaceo
judico
jungo
juvo
laboro
licet
ludo
maneo
moneo
narro
nego
nescio
nolo
nosco
occido
occupo
odi
oportet
opto
ostendo
parco
pareo
paro
pelo
perdo
permitto
pervenio
placeo
praebeo
praesto
prohibeo
prosum
quaero
quiesco
recipio
reddo
redeo
refero
relinquo
respondeo
rumpo
scio
scribo
sentio
soleo
specto
spero
statuo
sumo
taceo
tango
tego
terreo
timeo
tollo
traho
trado
valeo
veho
verto
veto
Proinde
concepta
rabie
saeviore
quam
desperatio
incendebat
et
fames
amplificatis
viribus
ardore
incohibili
in
excidium
urbium
matris
Seleuciae
efferebantur
quam
comes
tuebatur
Castricius
tresque
legiones
bellicis
sudoribus
induratae
Haec
ubi
latius
fama
vulgasset
missaeque
relationes
adsiduae
Gallum
Caesarem
permovissent
quoniam
magister
equitum
longius
ea
tempestate
distinebatur
iussus
comes
orientis
Nebridius
contractis
undique
militaribus
copiis
ad
eximendam
periculo
civitatem
amplam
et
oportunam
studio
properabat
ingenti
quo
cognito
abscessere
latrones
nulla
re
amplius
memorabili
gesta
dispersique
ut
solent
avia
montium
petiere
celsorum
Proinde
concepta
rabie
saeviore
quam
desperatio
incendebat
et
fames
amplificatis
viribus
ardore
incohibili
in
excidium
urbium
matris
Seleuciae
efferebantur
quam
comes
tuebatur
Castricius
tresque
legiones
bellicis
sudoribus
induratae
Haec
ubi
latius
fama
vulgasset
missaeque
relationes
adsiduae
Gallum
Caesarem
permovissent
quoniam
magister
equitum
longius
ea
tempestate
distinebatur
iussus
comes
orientis
Nebridius
contractis
undique
militaribus
copiis
ad
eximendam
periculo
civitatem
amplam
et
oportunam
studio
properabat
ingenti
quo
cognito
abscessere
latrones
nulla
re
amplius
memorabili
gesta
dispersique
ut
solent
avia
montium
petiere
celsorum